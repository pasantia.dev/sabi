<?php

$servername = "localhost";
$username = "admin";
$password = "";
$dbname = "proyecto_asistencias_profes";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}

$sql1 = "SELECT COUNT(*) AS cantidad FROM materias";
$resultado1 = mysqli_query($conn, $sql1);

if ($resultado1) {
    $cant = mysqli_fetch_assoc($resultado1);
    $cant_filas = $cant['cantidad'];
}

echo $cant_filas."<br>";

for ($i = 1; $i <= $cant_filas; $i++) {
    $sql2 = "SELECT profesor_cargo FROM materias WHERE materias.codigo = '$i'";
    $resultado2 = mysqli_query($conn, $sql2);

    if ($resultado2) {
        $nomb = mysqli_fetch_assoc($resultado2);
        $nombre = $nomb['profesor_cargo'];
    }

    $sql3 = "SELECT codigo FROM datos_profesores WHERE CONCAT(datos_profesores.apellido,' ',datos_profesores.nombre) LIKE '%$nombre%'";
    $resultado3 = mysqli_query($conn, $sql3);

    if ($resultado3) {
        $cod = mysqli_fetch_assoc($resultado3);
        $codigo = $cod['codigo'];

        // Verificar si el código es válido antes de la actualización
        if (!empty($codigo)) {
            $sql4 = "UPDATE materias SET codigo_profe = '$codigo' WHERE materias.codigo = '$i'";
            $resultado4 = mysqli_query($conn, $sql4);
        }
    }
}

// Cerrar la conexión
mysqli_close($conn);
?>