<?php
  // Conexión a la base de datos
  $servername = "localhost";
  $username = "admin";
  $password = "";
  $dbname = "proyecto_asistencias_profes";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  if(isset($_POST['opciones_curso'])){

    $opciones_curso = $_POST['opciones_curso'];
    $opciones_dia = $_POST['opciones_dia'];
    if ($opciones_dia == "semanal"){
        $sql = "SELECT materias.materia, materias.dia, materias.horario_entrada, materias.horario_salida, materias.profesor_cargo 
            FROM materias
            WHERE materias.curso = '$opciones_curso'";
    }else{
    $sql = "SELECT materias.materia, materias.dia, materias.horario_entrada, materias.horario_salida, materias.profesor_cargo 
            FROM materias
            WHERE materias.curso = '$opciones_curso' and materias.dia = '$opciones_dia'";
    }

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {

      // Almacenar los resultados en un array
      $results_array = array();

      while($row = $result->fetch_assoc()) {
        $results_array[] = array(
          "dia"=>$row["dia"],
          "materia"=>$row["materia"],
          "horario" => $row["horario_entrada"] . " - " . $row["horario_salida"],
          "profesor_cargo"=>$row["profesor_cargo"]
      );
      }
      
      // Enviar los resultados como respuesta en formato JSON
      header('Content-Type: application/json');
      echo json_encode($results_array);
    } else {
      echo "No se encontraron resultados para la fecha seleccionada.";
    }
  }
  
  // Cerrar la conexión a la base de datos
  $conn->close();
?>