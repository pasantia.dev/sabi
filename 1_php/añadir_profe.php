<?php

// Realizar la conexión a la base de datos
  $servername = "localhost";
  $username = "admin";
  $password = "";
  $dbname = "proyecto_asistencias_profes";

  $conn = mysqli_connect($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

// Obtén los datos POST directamente, no es necesario json_decode
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $cuil = $_POST["cuil"];
    $lunes = $_POST["lunes"];
    $martes = $_POST["martes"];
    $miercoles = $_POST["miercoles"];
    $jueves = $_POST["jueves"];
    $viernes = $_POST["viernes"];

    if ($lunes == "-" || $lunes == "" || $lunes == "00:00:00") {
        $lunes = null;
    }
    if ($martes == "-" || $martes == "" || $martes == "00:00:00") {
        $martes = null;
    }
    if ($miercoles == "-" || $miercoles == "" || $miercoles == "00:00:00") {
        $miercoles = null;
    }
    if ($jueves == "-" || $jueves == "" || $jueves == "00:00:00") {
        $jueves = null;
    }
    if ($viernes == "-" || $viernes == "" || $viernes == "00:00:00") {
        $viernes = null;
    }


    $sql = "INSERT INTO `datos_profesores` (`codigo`, `nombre`, `apellido`, `CUIL`, `lunes`, `martes`, `miercoles`, `jueves`, `viernes`)
    VALUES (NULL, '$nombre', '$apellido', '$cuil', ";

// Añadir los valores de los días (con o sin comillas según sea nulo o no)
$sql .= $lunes !== null ? "'$lunes'" : "NULL";
$sql .= ", ";
$sql .= $martes !== null ? "'$martes'" : "NULL";
$sql .= ", ";
$sql .= $miercoles !== null ? "'$miercoles'" : "NULL";
$sql .= ", ";
$sql .= $jueves !== null ? "'$jueves'" : "NULL";
$sql .= ", ";
$sql .= $viernes !== null ? "'$viernes'" : "NULL";

$sql .= ")";

$result = $conn->query($sql);

// Cerrar la conexión a la base de datos
$conn->close();
?>
