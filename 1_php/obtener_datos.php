<?php

$servername = "localhost";
$username = "admin";
$password = "";
$dbname = "proyecto_asistencias_profes";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}

// Consulta para obtener los datos de la base de datos
$consulta = "SELECT DISTINCT curso FROM materias;";
$resultado = mysqli_query($conn, $consulta);

// Crear un array para almacenar los datos
$datos = array();



// Recorrer los resultados y agregarlos al array
while ($fila = mysqli_fetch_assoc($resultado)) {
    $datos[] = $fila;
}

// Cerrar la conexión
mysqli_close($conn);

// Enviar los datos en formato JSON
header('Content-Type: application/json');
echo json_encode($datos);
?>