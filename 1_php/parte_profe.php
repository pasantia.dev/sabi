<?php

$servername = "localhost";
$username = "admin";
$password = "";
$dbname = "proyecto_asistencias_profes";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Conexión fallida: " . mysqli_connect_error());
}
$codigo = $_POST['codigo'];

$sql = "SELECT materias.curso, materias.materia, materias.dia, materias.horario_entrada, materias.horario_salida, materias.profesor_cargo 
        FROM materias 
        WHERE materias.codigo_profe = '$codigo'";


$result = $conn->query($sql);

if ($result->num_rows > 0) {

  // Almacenar los resultados en un array
  $resultado_array = array();

  while($row = $result->fetch_assoc()) {
    $resultado_array[] = array(
      "curso"=>$row["curso"],
      "materia"=>$row["materia"],
      "dia" => $row["dia"],
      "horario" => $row["horario_entrada"] . "   " . $row["horario_salida"],
  );
  }
  
  // Enviar los resultados como respuesta en formato JSON
  header('Content-Type: application/json');
  echo json_encode($resultado_array);
} else {
  echo "No se encontraron resultados para la fecha seleccionada.";
}
  
  // Cerrar la conexión a la base de datos
  $conn->close();


?>