# SABI (Sistema de Acceso Biométrico ITEL)

* El presente proyecto es desarrolado por alumnos del [ITEL](https://itel.edu.ar/) en el ámbito de las Prácticas Profesionalizantes de 7º año de su formación técnica en el nivel secundario. 
* La documentación se aloja en la [wiki del proyecto](https://gitlab.com/pasantia.dev/sabi/-/wikis/home)

## Contribuidores

| Año  | Mantenedor               | Email           |
| ---- | ------------------------ | --------------- |
| 2023 | Valentin Alejo GLUSZCZUK | @vagluszczuk    |
| 2024 | Matías ORELLANA          | @MatiasOrellana |
| 2024 | Marcos Rubén TORRES      | @mrtorres1      |







