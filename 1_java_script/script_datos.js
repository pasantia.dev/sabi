function postData() {
  const form = document.querySelector('form');
  const formData = new FormData(form);
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    console.log('readyState:', this.readyState);
    console.log('status:', this.status);

    if (this.readyState === 4 && this.status === 200) {
      const resultados_datos_profes = document.getElementById('resultados_datos_profes');
      resultados_datos_profes.innerHTML = '';
      const data = JSON.parse(this.responseText);
      if (data.length > 0) {
        const tableContainer = document.createElement('div');
        tableContainer.className = 'table-container'; // Agrega una clase CSS para el contenedor
        const table = document.createElement('table');
        table.className = 'tabla_datos';
        const thead = document.createElement('thead');
        const tbody = document.createElement('tbody');
        const headerRow = document.createElement('tr');
        const headers = ['Codigo', 'Nombre', 'Apellido', 'CUIL', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes'];
        headers.forEach(headerText => {
          const header = document.createElement('th');
          const textNode = document.createTextNode(headerText);
          header.appendChild(textNode);
          headerRow.appendChild(header);
        });
        thead.appendChild(headerRow);
        data.forEach(rowData => {
          const row = document.createElement('tr');
          Object.values(rowData).forEach(value => {
            const cell = document.createElement('td');
            const textNode = document.createTextNode(value);
            cell.appendChild(textNode);
            row.appendChild(cell);
          });

          // Agregar identificador único a la fila
          row.setAttribute('data-id', rowData.codigo);

          // Agregar evento de clic a la fila
          row.addEventListener('click', function() {
            const codigo = this.getAttribute('data-id');
            resaltarFilaSeleccionada(this);
            llamarArchivoPHP(codigo);
          });

          tbody.appendChild(row);
        });
        table.appendChild(thead);
        table.appendChild(tbody);
        tableContainer.appendChild(table);
        resultados_datos_profes.appendChild(tableContainer);
      } else {
        resultados_datos_profes.textContent = 'No se encontraron resultados para la fecha seleccionada.';
      }
    }
  };
  xhr.open('POST', form.action, true);
  xhr.send(formData);
}

// Agregar evento de escucha al input de tipo texto
const input = document.getElementById('apellido');
input.addEventListener('input', function() {
  // Llamar a la función postData() cuando cambia el valor del input
  postData();
});

function resaltarFilaSeleccionada(row) {
  const filas = document.querySelectorAll('tr[data-id]');
  filas.forEach(fila => fila.classList.remove('seleccionada'));
  row.classList.add('seleccionada');
}

function llamarArchivoPHP(codigo) {
  $.ajax({
    url: '../1_php/parte_profe.php',
    method: 'POST',
    data: { codigo: codigo },
    success: function(response) {
      // Manejar la respuesta del archivo PHP
      mostrarTabla(response);
    },
    error: function(xhr, status, error) {
      // Manejar errores en la solicitud AJAX
      console.error(error);
    }
  });
}

function mostrarTabla(data) {
  const resultados_parte_profes = document.getElementById('resultados_parte_profes'); // Cambio de id a 'resultados_parte_profes'
  resultados_parte_profes.innerHTML = ''; // Cambio de id a 'resultados_parte_profes'

  if (data.length > 0) {
    const tableContainer = document.createElement('div');
    tableContainer.className = 'table-container';

    const table = document.createElement('table');
    const thead = document.createElement('thead');
    const tbody = document.createElement('tbody');

    const headers = ['Curso', 'Materia', 'Día', 'Horario'];

    const headerRow = document.createElement('tr');
    headers.forEach(headerText => {
      const header = document.createElement('th');
      const textNode = document.createTextNode(headerText);
      header.appendChild(textNode);
      headerRow.appendChild(header);
    });
    thead.appendChild(headerRow);

    data.forEach(rowData => {
      const row = document.createElement('tr');

      Object.values(rowData).forEach(value => {
        const cell = document.createElement('td');
        const textNode = document.createTextNode(value);
        cell.appendChild(textNode);
        row.appendChild(cell);
      });

      tbody.appendChild(row);
    });

    table.appendChild(thead);
    table.appendChild(tbody);
    tableContainer.appendChild(table);
    resultados_parte_profes.appendChild(tableContainer); // Cambio de id a 'resultados_parte_profes'
  } else {
    resultados_parte_profes.textContent = 'No se encontraron resultados para la fecha seleccionada.'; // Cambio de id a 'resultados_parte_profes'
  }
}

var image = document.getElementById("myImage");

var isImage1 = true;

function cambiarImagen() {
  if (isImage1) {
    image.src = "../img/disco-flexible.png";
    isImage1 = false;
  } else {
    image.src = "../img/lapiz.png";
    isImage1 = true;
  }
}

let isEditMode = false; // Variable para rastrear el modo de edición
let originalCellValue = ''; // Variable para almacenar el valor original de la celda
let editingRow = null; // Variable para almacenar la fila que se está editando


function toggleEditMode() {
  const selectedRow = document.querySelector('tr.seleccionada');
  const parte = document.getElementById("resultados_parte_profes");
  const div_profe = document.getElementById("resultados_datos_profes");


  if (selectedRow) {
    const cells = selectedRow.querySelectorAll('td');
    const rowData = {}; // Objeto para almacenar los datos de la fila

    if (!isEditMode) {
      // Entrar en el modo de edición
      cells.forEach((cell, index) => {
        if (index !== 0) { // Evitar editar la primera celda (código)
          originalCellValue = cell.textContent;
          cell.innerHTML = `<input type="text" value="${originalCellValue}">`;
        }
      });
      isEditMode = true;

      // Ocultar el parte del profe
      parte.style.display = 'none';
      div_profe.style.left = '47.5%';

      // Deshabilitar la interacción con las filas no editadas
      const allRows = document.querySelectorAll('tr[data-id]');
      allRows.forEach(row => {
        if (row !== selectedRow) {
          row.classList.add('fila-no-editable');
        }
      });
    } else {
      // Salir del modo de edición y guardar los cambios
      cells.forEach((cell, index) => {
        if (index !== 0) { // Evitar editar la primera celda (código)
          const newValue = cell.querySelector('input').value;
          cell.innerHTML = newValue;

          // Obtener el encabezado correspondiente al índice
          const headers = document.querySelectorAll('thead th');
          const headerText = headers[index].textContent;
          const codigo = selectedRow.getAttribute('data-id');

          rowData["codigo"] = codigo;
          // Almacena los datos en el objeto rowData con el nombre del encabezado
          rowData[headerText] = newValue;
          
        }
      });
      isEditMode = false;

      // Restaurar la interacción con todas las filas
      const allRows = document.querySelectorAll('tr[data-id]');
      allRows.forEach(row => {
        row.classList.remove('fila-no-editable');
      });

      // Mostrar el array de datos editados en la consola
      console.log('Datos editados:', rowData);

      // Mostrar nuevamente el parte del profe
      parte.style.display = 'block';
      div_profe.style.left = '35%';

      // Enviar los datos actualizados al servidor PHP para guardarlos
      guardarDatosActualizados(rowData);
      postData();
    }
  }
}

function guardarDatosActualizados(data) {
  // Realiza una solicitud AJAX para guardar los datos en el servidor PHP
  $.ajax({
    url: '../1_php/edicion_profes.php',
    method: 'POST',
    data: data,
    success: function(response) {
      // Manejar la respuesta del servidor si es necesario
      console.log('Datos guardados exitosamente');
      // Llama a la función postData después de guardar los datos
      postData();
    },
    error: function(xhr, status, error) {
      // Manejar errores en la solicitud AJAX
      console.error(error);
    }
  });
}


$(document).ready(function() {
  // Manejar el clic del botón
  $('#boton_eliminar_fila').on('click', function() {
      const selectedRow = document.querySelector('tr.seleccionada');
      const codigo = selectedRow.getAttribute('data-id');
      const cod = {}; 
      cod["codigo"] = codigo;
    // Mostrar un mensaje de confirmación
    if (confirm('¿Estás seguro de que desea a este profesor?')) {
      if (confirm('este profesor sera eliminado permanentemente del la base de datos,¿desea continuar?')) {
      // El usuario confirmó, realizar la solicitud AJAX al archivo PHP
      $.ajax({
        url: '../1_php/eliminar_profe.php',
        method: 'POST',
        data:cod,
        success: function(data) {
          // Realiza cualquier acción necesaria con la respuesta del archivo PHP
          console.log('Respuesta del archivo PHP:', data);

          // Llama a la función postData
          postData();
        },
        error: function(xhr, status, error) {
          // Maneja errores en la solicitud AJAX
          console.error(error);
        }
      });
    } else {
      // El usuario canceló, no se realiza la solicitud
      console.log('El usuario canceló la solicitud.');
    }
    } else {
      // El usuario canceló, no se realiza la solicitud
      console.log('El usuario canceló la solicitud.');
    }
  });
});

function añadir_profe(){

  // Ocultar los divs que quieres ocultar
  document.querySelector(".buscador_datos").style.display = "none";
  document.querySelector("#resultados_datos_profes").style.display = "none";
  document.querySelector("#resultados_parte_profes").style.display = "none";

  // Mostrar el div "añadir_profe"
  document.querySelector(".añadir_profe").style.display = "block";

}

function cancelar_añadir_profe(){
  
  // Mostrar los divs que quieres Mostrar
  document.querySelector(".buscador_datos").style.display = "block";
  document.querySelector("#resultados_datos_profes").style.display = "block";
  document.querySelector("#resultados_parte_profes").style.display = "blolck";

  // Ocultar el div "añadir_profe"
  document.querySelector(".añadir_profe").style.display = "none";
}

$(document).ready(function() {
  // Manejar el envío del formulario
  $('#añadir_profe').on('submit', function(event) {
      event.preventDefault(); // Evitar el envío predeterminado del formulario

      // Crear un objeto FormData con los datos del formulario
      var formData = new FormData(this);

      console.log(formData);

      $.ajax({
          url: '../1_php/añadir_profe.php',
          method: 'POST',
          data: formData,
          contentType: false, // Importante: deshabilitar la configuración automática del tipo de contenido
          processData: false, // Importante: deshabilitar el procesamiento automático de datos
          success: function(data) {
              // Realiza cualquier acción necesaria con la respuesta del archivo PHP
              console.log('Respuesta del archivo PHP:', data);

              // Llama a la función postData
              cancelar_añadir_profe();
              postData();
          },
          error: function(xhr, status, error) {
              // Maneja errores en la solicitud AJAX
              console.error(error);
          }
      });
  });
});
